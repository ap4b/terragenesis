package com.terraformars.Population;
import com.terraformars.function.*;
public class Population {
	
	protected int numberPopulation;
	
	protected double happiness;
	protected double foodConsumption;
	protected double DrinkingWaterConsumption;
	
	public double breed()
	{
		return (this.numberPopulation*( (4/100)+ Math.random()*0.4 + Function.gauss( (happiness*6/100)-3, 0, Math.sqrt(3) )/100) )  ;
	}
	
	public double death()
	{
		return this.numberPopulation*( (4/100)+Math.random()*0.4);
	}
	
	
	

}
