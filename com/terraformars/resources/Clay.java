package com.terraformars.resources;

public class Clay extends Mining {
	
	public Clay() {
		this.number=0;
		this.extraction= new LevelExtraction_I();
	}
	
	public Clay(double number, ExtractionLevel extraction){
		
		this.number=number; 
		this.extraction=extraction;
	}
	

}
