package com.terraformars.resources;

public class Potash extends Mining{
	
	public Potash() {
		this.number=0;
		this.extraction= new LevelExtraction_I();
	}
	
	public Potash(double number, ExtractionLevel extraction){
		
		this.number=number; 
		this.extraction=extraction;
	}
}
