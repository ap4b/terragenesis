package com.terraformars.resources;

public class Limestone extends Mining{
	
	public Limestone() {
		this.number=0;
		this.extraction= new LevelExtraction_I();
	}
	
	public Limestone(double number, ExtractionLevel extraction){
		
		this.number=number; 
		this.extraction=extraction;
	}

}
