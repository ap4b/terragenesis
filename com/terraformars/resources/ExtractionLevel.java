package com.terraformars.resources;

public interface ExtractionLevel {
	public double extract();

}
