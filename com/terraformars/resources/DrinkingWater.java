package com.terraformars.resources;

public class DrinkingWater extends Hydraulic {
	
	public DrinkingWater() {
		this.number=0;
		this.extraction= new LevelExtraction_I();
	}
	
	public DrinkingWater(double number, ExtractionLevel extraction){
		
		this.number=number; 
		this.extraction=extraction;
	}
	
	public double toExtract(){
		
		double change=this.number;
		change-=extraction.extract();
		
		if(change<0) 
		{ 
			return -1;
		} else { 
			this.number=change;
			return extraction.extract();
		}
		
	}
	

}
