package com.terraformars.resources;

public abstract class Resource {
	
	protected double number;
	protected ExtractionLevel extraction;

	
	public double getNumber() {
		return this.number;
	}
	public double toExtract(){
		
		double change=this.number;
		change-=extraction.extract();
		
		if(change<0) 
		{ 
			return -1;
		} else { 
			this.number=change;
			return extraction.extract();
		}
		
	}
	
	protected void generateQuantity() {
		this.number= ( Math.random() )*50000;
	}
	
	public void levelUp() {
			
		if( this.extraction.getClass().equals( new LevelExtraction_III() ) ) { this.extraction= new LevelExtraction_IV(); }
		if( this.extraction.getClass().equals( new LevelExtraction_II()  ) ) { this.extraction= new LevelExtraction_III(); }
		if( this.extraction.getClass().equals( new LevelExtraction_I()   ) ) { this.extraction= new LevelExtraction_II(); }
	}
		
	public void levelDown() {
				
		if( this.extraction.getClass().equals( new LevelExtraction_II() ) ) { this.extraction= new LevelExtraction_I(); }
		if( this.extraction.getClass().equals( new LevelExtraction_III()  ) ) { this.extraction= new LevelExtraction_II(); }
		if( this.extraction.getClass().equals( new LevelExtraction_IV()   ) ) { this.extraction= new LevelExtraction_III(); }
	}
}
