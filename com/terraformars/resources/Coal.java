package com.terraformars.resources;

public class Coal extends Mining{
	
	public Coal() {
		this.number=0;
		this.extraction= new LevelExtraction_I();
	}
	
	public Coal(double number, ExtractionLevel extraction){
		
		this.number=number; 
		this.extraction=extraction;
	}
	
}
