package com.terraformars.resources;

public class Iron extends Mining{
	
	
		public Iron() {
			this.number=0;
			this.extraction= new LevelExtraction_I();
		}
		
		public Iron(double number, ExtractionLevel extraction){
			
			this.number=number; 
			this.extraction=extraction;
		}
					
}
		
		
	
