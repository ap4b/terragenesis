package com.terraformars.function;
import java.math.*;

public class Function {
	
	static public double gauss(double x, double mu, double sigma) {
		return ( 1/( sigma*Math.sqrt(2*Math.PI) )* Math.exp( (-1/2)*Math.pow( (x-mu)/sigma ,2) ) ); 
	}

}
