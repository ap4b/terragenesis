package com.terraformars.planet;
import com.terraformars.resources.*;

import java.util.*;

public class Planet {

	protected ArrayList<Resource> ressource = new ArrayList<Resource>();
	
	public void addIronVein() { this.ressource.add( new Iron() ); }
	public void addClayVein() { this.ressource.add( new Clay() ); }
	public void addLimestoneVein() { this.ressource.add( new Limestone() ); }
	public void addCoalVein() { this.ressource.add( new Coal() ); }
	public void addDrinkingWaterSource() { this.ressource.add( new DrinkingWater() ); }
	



	
}
